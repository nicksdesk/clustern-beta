import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { auth } from 'firebase/app';
import { User } from 'firebase';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private user: User;

  constructor(private router: Router, public afAuth: AngularFireAuth, private store: AngularFirestore) {
    this.afAuth.authState.subscribe(user => {
      if (user) {
        this.user = user;
        localStorage.setItem('clustern-user', JSON.stringify(this.user));
      } else {
        localStorage.setItem('clustern-user', null);
      }
    });
  }

  async loginWithGoogle() {
    try {
      await this.afAuth.auth.signInWithPopup(new auth.GoogleAuthProvider());
      this.router.navigate(['/dashboard']);
    } catch (e) {
      console.log(e.message);
    }
  }

  async loginWithTwitter() {
    try {
      /*await this.afAuth.auth.signInWithPopup(new auth.TwitterAuthProvider());
      this.router.navigate(['/dashboard']);*/
      alert('Twitter support coming soon...');
    } catch (e) {
      console.log(e.message);
    }
  }

  async loginWithGithub() {
    try {
      await this.afAuth.auth.signInWithPopup(new auth.GithubAuthProvider());
      this.router.navigate(['/dashboard']);
    } catch (e) {
      console.log(e.message);
    }
  }

  async logout() {
    try {
      await this.afAuth.auth.signOut();
      localStorage.removeItem('clustern-user');
      this.router.navigate(['/my']);
    } catch (e) {
      console.log(e.message);
    }
  }

  getUserRecognizableString(): string {
    const user = JSON.parse(localStorage.getItem('clustern-user'));
    if (user != null) return (user.displayName !== '') ? user.displayName : user.email;
  }

  get isLoggedIn(): boolean {
    const user = JSON.parse(localStorage.getItem('clustern-user'));
    return user !== null;
  }

}
