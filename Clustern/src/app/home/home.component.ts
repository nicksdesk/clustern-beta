import { Component, OnInit } from '@angular/core';

declare var $: any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    $(document).ready(() => {
      $('.sidenav').sidenav();
      $('.parallax').parallax();
      $('.sidenav-overlay').on('click', () => {
        $('.ham').removeClass('active'); 
      });
    });
  }

}
