import { Component, OnInit } from '@angular/core';
import { AuthService } from '../_auth/auth.service';

@Component({
  selector: 'app-my',
  templateUrl: './my.component.html',
  styleUrls: ['./my.component.scss']
})
export class MyComponent implements OnInit {

  constructor(private auth: AuthService) {}

  login(type: string) {
    switch(type) {
      case "google":
        this.auth.loginWithGoogle();
      break;
      case "twitter":
        this.auth.loginWithTwitter();
      break;
      case "github":
        this.auth.loginWithGithub();
      break;
      default:
        //no valid login method passed through type
      break;
    }
  }

  logout() {
    this.auth.logout();
  }

  ngOnInit() {}

}
