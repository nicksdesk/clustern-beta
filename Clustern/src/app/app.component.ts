import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

declare var $: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'Clustern';

  constructor(public router: Router) {}

  ngOnInit() {
    $(document).ready(() => {
      $('.sidenav').sidenav();
      $('.parallax').parallax();
      $('.sidenav-overlay').on('click', () => {
        $('.ham').removeClass('active');
      });
      $('ul.tabs').tabs();
    });
  }
}
