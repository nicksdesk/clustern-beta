import { Component, OnInit } from '@angular/core';
import { AuthService } from '../_auth/auth.service';
import { Router } from '@angular/router';

declare var $: any;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(public auth: AuthService, public router: Router) {}

  logout() {
    this.auth.logout();
  }

  getMobileTitle(): string {
    if (this.router.url.replace('/', '') === '') {
      return this.router.url.replace('/', 'home');
    }
    return this.router.url.replace('/', '');
  }

  ngOnInit() {
    $(document).ready(() => {
      $('ul.tabs').tabs();
      $('.sidenav').sidenav();
      $('.parallax').parallax();
      $('#dash_deploys').focus();
      $('.sidenav-overlay').on('click', () => {
        $('.ham').removeClass('active');
      });
    });
  }

}
