import { Component, OnInit, Renderer2, ViewChild, ElementRef } from '@angular/core';
import { AuthService } from '../_auth/auth.service';
import { Router } from '@angular/router';
import { auth } from 'firebase';

declare var $: any;

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  // tslint:disable-next-line: no-shadowed-variable
  constructor(public auth: AuthService, private router: Router, private render: Renderer2) {}

  username: string = this.auth.getUserRecognizableString();

  login() {
    this.router.navigate(['/my']);
  }

  /*
    TODO: FIX
    Currently experiencing and error that is freezing the elements when the raw html of the squares is
    appended back to the screen. I think its a problem with the ViewChild not finding the correct div
    after readding it.

    TODO: ADD
    1 - add animations between screen changes of the different dashboard buttons
    2 - possibly implement the ability for mobile devices to use the dashboard
    3 - color match the buttons corresponding status tile
  */

  ngOnInit() {
    $(document).ready(() => {
      $('ul.tabs').tabs();
      $('#dash_deploys').focus();
    });
  }

}
