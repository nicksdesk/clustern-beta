import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import * as rxjs from 'rxjs';

import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFirestoreModule } from '@angular/fire/firestore';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MyComponent } from './my/my.component';
import { PricingComponent } from './pricing/pricing.component';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { DashboardComponent } from './dashboard/dashboard.component';

const fbConfig = {
  apiKey: 'AIzaSyCBBuXsDrtTlnGLDAOJ1ZkLBtnAOKjsmII',
  authDomain: 'nicklvsa7802.firebaseapp.com',
  databaseURL: 'https://nicklvsa7802.firebaseio.com',
  projectId: 'nicklvsa7802',
  storageBucket: 'nicklvsa7802.appspot.com',
  messagingSenderId: '237845857252',
  appId: '1:237845857252:web:dee4d4ee2d25cfcf'
};

@NgModule({
  declarations: [
    AppComponent,
    MyComponent,
    PricingComponent,
    HomeComponent,
    AboutComponent,
    HeaderComponent,
    FooterComponent,
    DashboardComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(fbConfig),
    AngularFireAuthModule,
    AngularFirestoreModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
