$(document).ready(() => {

    $('.sidenav').sidenav();
    $('.parallax').parallax();

    $('.sidenav-overlay').on('click', () => {
        $('.ham').removeClass('active'); 
    });

});