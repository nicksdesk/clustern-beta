# Clustern


## About:

### Clustern is a custom code deployment software solution that can deploy to different services like digital ocean and vultr.


## How it works:


### Clustern works by combining multiple technologies from different services like digital ocean and vultr and making them act as one service. This allows developers for easier integration between platforms. The backend is written entirely in Go and the frontend is Angular. This allows the platform to be very responsive and easy to use!


## Why?


### Clustern was devleoped for the developers that like trying different services for the applications. Clustern allows many different features including load balancing between digital ocean and vultr.


## Development


### Clustern is developed using Go and Angular. It is written mostly using VSCode and then pushed to Gitlab. It is then automatically deployed to a netlify instance for testing as well as my local server in a docker container.

## Other Information


### Currently, Clustern is using Materialize as the style framework and I was running into issues with Angular 8 and the node module for Materialize. I am pretty much just using vanilla css linking in the index file until I can figure the issue out and use a true Angular imported library.


## Usage & Deployment


### Docs coming soon...          

###### * [Auto Deployed beta preview for clustern](https://clustern-beta.nicksdesk.com/clustern) * 
