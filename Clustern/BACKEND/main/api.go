package main

import (
	"log"
	"net/http"

	shared "./shared"
	"github.com/gorilla/mux"
)

func startHandlers(done chan bool) {
	routes := new(shared.RouteModel)
	r := mux.NewRouter().StrictSlash(true)

	//Handle the base endpoints if no data is provided
	r.HandleFunc("/", routes.BaseHandler)
	r.HandleFunc("/api", routes.BaseAPIHandler)

	//Handle valid api subroutes
	apiHandler := r.PathPrefix("/api").Subrouter()
	getHandler := apiHandler.PathPrefix("/get").Subrouter()
	createHandler := apiHandler.PathPrefix("/create").Subrouter()
	deleteHandler := apiHandler.PathPrefix("/delete").Subrouter()

	//Handle GET endpoints
	getHandler.HandleFunc("/", routes.BaseAPIGetHandler)

	//Handle CREATE endpoints
	createHandler.HandleFunc("/", routes.BaseAPICreateHandler)

	//Handle DELETE endpoints
	deleteHandler.HandleFunc("/", routes.BaseAPIDeleteHandler)

	//Begin http handler
	http.Handle("/", r)

	done <- true
}

func main() {
	done := make(chan bool)
	go startHandlers(done)
	log.Fatal(http.ListenAndServe(":4201", nil))
	<-done
}
