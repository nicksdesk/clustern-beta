package shared

import "time"

//BackendVersion - current version of the backend
const BackendVersion = 0.1

var (
	//ERRORS - possible clustern error responses that could be returned
	ERRORS = make(map[string]error)
)

//Context - carry data between go routines
type Context interface {
	Done() <-chan struct{}
	Err() error
	Deadline() (deadline time.Time, valid bool)
}

//RouteModel - model of all clustern routes
type RouteModel struct {
	Name string
	Path string
}
