package shared

import (
	"net/http"
)

//BaseHandler - Handles any call to "/"
func (route *RouteModel) BaseHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.Write([]byte("{'response': 'invalid_endpoint_data', 'data': '/'}"))

}

//BaseAPIHandler - Handles any call to "/api"
func (route *RouteModel) BaseAPIHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.Write([]byte("{'response': 'invalid_endpoint_data', 'data': '/api'}"))
}

//BaseAPIGetHandler - Handles any call to "/api/get" with no params
func (route *RouteModel) BaseAPIGetHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.Write([]byte("{'response': 'invalid_endpoint_data', 'data': '/api/get'}"))

}

//BaseAPICreateHandler - Handles any call to "/api/create" with no params
func (route *RouteModel) BaseAPICreateHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.Write([]byte("{'response': 'invalid_endpoint_data', 'data': '/api/create'}"))

}

//BaseAPIDeleteHandler - Handles any call to "/api/delete" with no params
func (route *RouteModel) BaseAPIDeleteHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.Write([]byte("{'response': 'invalid_endpoint_data', 'data': '/api/delete'}"))

}
