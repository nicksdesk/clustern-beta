package main

import (
	"context"

	"github.com/digitalocean/godo"
	"golang.org/x/oauth2"
)

//TokenSource - AccessToken:string
type TokenSource struct {
	AccessToken string
}

//Token - returns [token, error]
func (t *TokenSource) Token() (*oauth2.Token, error) {
	token := &oauth2.Token{
		AccessToken: t.AccessToken,
	}
	return token, nil
}

//CreateDroplet - returns a droplet (on success) or error
func CreateDroplet(name string, region string, size string, slug string, client godo.Client) (*godo.Droplet, error) {
	createRequest := &godo.DropletCreateRequest{
		Name:   name,
		Region: region,
		Size:   size,
		Image: godo.DropletCreateImage{
			Slug: slug,
		},
	}

	ctx := context.TODO()
	newDroplet, _, err := client.Droplets.Create(ctx, createRequest)

	if err != nil {
		return nil, err
	}

	return newDroplet, nil
}

//GetDroplets - returns [list, error]
/*
	thx digital ocean for having awesome docs
*/
func GetDroplets(ctx context.Context, client *godo.Client) ([]godo.Droplet, error) {
	list := []godo.Droplet{}
	opt := &godo.ListOptions{}

	for {
		droplets, resp, err := client.Droplets.List(ctx, opt)
		if err != nil {
			return nil, err
		}

		for _, d := range droplets {
			list = append(list, d)
		}

		if resp.Links == nil || resp.Links.IsLastPage() {
			break
		}

		page, err := resp.Links.CurrentPage()
		if err != nil {
			return nil, err
		}

		opt.Page = page + 1
	}
	return list, nil
}
