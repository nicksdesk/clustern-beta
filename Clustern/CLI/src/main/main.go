package main

import (
	"fmt"
	"os"
	"strings"

	"github.com/pkg/browser"
)

func main() {
	args := os.Args
	if len(args) > 1 {
		for _, arg := range args {
			if strings.HasPrefix(strings.TrimSpace(arg), "--") {
				switch arg {
				case "add":
					fmt.Println("Add Test")
					break
				case "remove":
					fmt.Println("Remove Test")
					break
				case "update":
					fmt.Println("Update Test")
					break
				default:
					ThrowExcept("Error: Please use a valid -- argument!")
					break
				}
			} else {
				switch strings.TrimSpace(arg) {
				case "help":
					fmt.Println("Clustern Help")
					fmt.Println("-- help coming here soon... --")
				case "clustern":
					browser.OpenURL("https://clustern-beta.nicksdesk.com/clustern")
					break
				case "quit":
					os.Exit(0)
					break
				default:
					ThrowExcept("Error: Please use a valid argument!")
					break
				}
			}
		}
	} else {
		ThrowExcept("Error: Please provide a valid amount of arguments!")
	}
}
