package main

//ProjectConfig - The structure for a valid Clustern project deployment
type ProjectConfig struct {
	Title       string `json:"title"`
	Description string `json:"description"`
	Identifier  struct {
		Hash string `json:"hash"`
		Time string `json:"time"`
	} `json:"identifier"`
	ClusternUser struct {
		Username   string `json:"username"`
		Identifier struct {
			Hash string `json:"hash"`
			Time string `json:"time"`
		} `json:"identifier"`
	} `json:"clusternUser"`
	FileConfig struct {
		Name       string `json:"name"`
		Size       int    `json:"size"`
		ProjectDir string `json:"projectDir"`
	} `json:"fileConfig"`
	RequestedPlatform *ClusternPlatformType `json:"requestedPlatform"`
}

//ClusternPlatformType - The structure will allow projects to conform to a specific hosting platform (digitalocean, vultr) and load balance
type ClusternPlatformType struct {
	Name       string `json:"name"`
	Identifier struct {
		ClusternIdentifierShort string `json:"clusternIdentifierShort,omitempty"`
		ClusternIdentifierFull  string `json:"clusternIdentifierFull"`
	} `json:"identifier"`
	ClusternBalancer struct {
		Auto bool `json:"auto"`
		/*
			TODO: Allow for manual load balancing options when "Auto" is set to false
		*/
	} `json:"clusternBalancer,omitempty"`
	ClusternTeamIndetifier struct {
		ClusternIdentifierShort string `json:"clusternIdentifierShort"`
		ClusternIdentifierFull  string `json:"clusternIdentifierFull"`
	}
}
