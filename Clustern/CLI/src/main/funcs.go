package main

import (
	"fmt"
	"log"
	"os"
)

//ThrowExcept - commit an error message and quit
func ThrowExcept(msg string) {
	log.Fatal(msg)
	os.Exit(0)
}

//SubmitProjectQueue - submit a project to the current queue pool
func SubmitProjectQueue(project *ProjectConfig) (status string, err error) {
	fmt.Println("test")
	return "test", nil
}

//DeployQueuedProject - Deploy the current project in the queue pool
func DeployQueuedProject() (status string, err error) {
	//deploymentTime := time.Now()
	return "test", nil
}
